﻿## [Repo na Codebergu](https://codeberg.org/Informatyka2018/MakeMyDay)

<br>

## Spotkania online

[Snopyta](https://talk.snopyta.org/MakeMyDay)

[Disroot](https://calls.disroot.org/MakeMyDay)

<br>

## Źródła ikon

[Phosphor Icons](https://phosphoricons.com/) `<-- Jak czegoś nie ma tutaj, to szukać w linku poniżej`

[Remix Icon](https://remixicon.com/)