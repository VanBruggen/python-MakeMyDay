﻿[TOC]

## Kolejność działań

1. Zróbcie rzeczy z sekcji "Git".

1. Schemat bazy danych, z uwzględnieniem [dozwolonych typów](https://www.sqlite.org/datatype3.html):

    * Przed uruchomieniem kodu baza musi istnieć, tzn. tabela `TasksTable` (z root taskiem, o czym piszę poniżej) i tabela `TimeblocksTable`,

    * w tabeli `TasksTable` dodać kolumnę `sortMode` typu INTEGER,
    
    * w tabeli `TasksTable` dodać jeden rekord (nasz root task) z `id = 1`, `parentID = -1`, `sortMode = 1` i resztą pól = `NULL`.

1. Zapisywanie listy tasków i timeblocków do bazy (slot `save` w `mainWindow`, aktywowany po każdej zmianie w liście tasków lub timeblocków, np. dodanie elementu, usunięcie elementu, zmiana parametru elementu) i odtwarzanie tych list z bazy (konstruktor `mainWindow`, bo tylko przy uruchomieniu programu). Funkcje do obsługi tych działań zakodzone w module `model.py`.

1. Rzeczy do zrobienia w konstruktorze `mainWindow`.

1. Rzeczy do zrobienia w pozostałych modułach.

1. Sloty w `mainWindow`.

<br>

## Git

<br>

#### Instalacja

1. https://github.com/git-for-windows/git/releases/download/v2.34.1.windows.1/Git-2.34.1-64-bit.exe

1. Po instalacji odpalcie program "Git Bash" i sprawdźcie, czy git działa:

    ```
    git --version
    ```

<br>

#### Konfiguracja (Git Bash)

1. Ustawienie nazwy użytkownika:

    ```
    git config --global user.name "wasza-nazwa"
    ```

2. Ustawienie e-maila (dajcie ten, którego użyjecie przy tworzeniu konta na Codebergu):

    ```
    git config --global user.email "nazwa@email.com"
    ```

3. (Nie musicie rozumieć o co w tym chodzi, ale to ważne) Ustawienie automatycznej konwersji CRLF (Windows) na LF (Mac/Linux) i na odwrót:

    ```
    git config --global core.autocrlf true
    ```

4. Ustawienie "Notatnika" jako domyślnego edytora tekstu (jeśli wolicie inny, to [tutaj](https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config) na dole jest pełna tabela, ale PyCharma tam nie ma):

    ```
    git config --global core.editor notepad
    ```

5. Ustawienie kolorowania składni gita w konsoli:

    ```
    git config color.ui true
    ```

<br>

#### Tutorial (50 minut, same niezbędne rzeczy krok po kroku)

1. [Czym jest Git?](https://www.youtube.com/watch?v=D6EI7EbEN4Q) (5 minut)

1. [Git Tutorial for Beginners: Learn Git in 1 Hour](https://www.youtube.com/watch?v=8JJ101D3knE) (Potrzebne rozdziały, łącznie 35 minut)
    
    * od `Git Workflow` do `Renaming or moving files`

    * `Short Status`

    * `Viewing the Staged and Unstaged Changes`

    * `Viewing the history`

    * od `Unstaging files` do końca. 
    
1. [Czym jest branch?](https://www.youtube.com/watch?v=I8lQK0NK0lY) <-- (tu tylko do 5:00)
    
1. [Zdalne repozytorium](https://www.youtube.com/watch?v=7myDXGfEnds) <-- (tutaj do 4:58)
    
<br>

#### [Reference](https://backlog.com/git-tutorial/reference/basic-operations/)

<br>

#### [Cheat sheet](https://programmingwithmosh.com/wp-content/uploads/2020/09/Git-Cheat-Sheet.pdf)

<br>

#### Połączenie z repo

1. Utwórzcie konto na Codebergu:

    https://codeberg.org/
    
1. Podajcie mi swój nick na Messie.

1. Jak dodam was do teamu, to wejdźcie tutaj:

    https://codeberg.org/Informatyka2018/MakeMyDay
    
1. Skopiujcie adres https stąd:

    <media-tag src="https://cryptpad.disroot.org/blob/d8/d8d1750d76e0272fd3617ca896884e666fd66e3706e4a0cc" data-crypto-key="cryptpad:fYmhDNeRDKMh5hLAWeYPgyGo1APEpr4xK/Z8iuJA3PA="></media-tag>

1. Odpalcie PyCharma, zamknijcie dotychczasowy projekt i w oknie, które się pojawiło kliknijcie "Get from VCS":

1. Wklejcie skopiowany w punkcie 4 adres, ustawcie folder, do którego mają zostać pobrane pliki z repo i kliknijcie "Clone":

    <media-tag src="https://cryptpad.disroot.org/blob/63/63bed5e9ebd1846fedd97bce9e6b5dd80d9f2845863ac80d" data-crypto-key="cryptpad:dtDDi1pPPUidYMlIFwGOMzCKL5Ln2KmLh0+ZdL2v3PU="></media-tag>
    
1. Pewnie poprosi was o login i hasło do Codeberga.

1. Przełączcie się na brancha ze swoim imieniem (tzw. checkout).

    <media-tag src="https://cryptpad.disroot.org/blob/bf/bf052eba33cde58a9a97f742189fed155edc3c680bf0fc83" data-crypto-key="cryptpad:QHS/m7utta5fbF9XekqipTyNZNO8kDePTSjw/+yJ6U0="></media-tag>

1. Od teraz tylko tutaj commitujecie i pushujecie swoje zmiany. Ja zajmę się mergowaniem.

<br>

## Plan dla każdego modułu (WIP)

* `mainWindow`:

    * Konstruktor:

        * Inicjalizacja `self.chosenWeek` jako obecny tydzień.

        * Pole z listą (początkowo pustą) wszystkich bloków czasu (łatwiejsza serializacja i deserializacja).
        
        * Pole z listą (początkowo pustą) wszystkich tasków (j.w.).
        
        * Inicjalizacja obu powyższych list z bazy, w tym odtworzenie powiązań task --> parentTask oraz timeblock --> task.
        
        * Inicjalizacja `self.greatestTaskID` i `self.greatestTimeblockID` na podstawie największej wartości `self.modelTaskList[x].id` oraz `self.modelTimeblockList[y].id` (obie o jeden większe od znalezionych).
        
    * Sloty:
    
        * Tryb edycji w szczegółach zadania:

            * Schowanie przycisku trybu edycji

            * Pokazanie przycisków zatwierdzenia i anulowania

            * Pokazanie przycisku dodawania nowego Timeblocka

            * Pokazanie przycisków do edycji w każdym Timeblocku    
        
        * Dodanie zadania.
        
        * Ładowanie szczegółów zadania klikniętego w eksploratorze i aktualizacja paska lokalizacji po prawej.
        
        * Edycja zadania.
        
        * Przeniesienie zadania.
        
        * Skopiowanie zadania.        
        
        * Usunięcie zadania.
        
        * Przechodzenie do wskazanego zadania (`enterButton`) i aktualizacja paska lokalizacji po lewej.
        
        * Przechodzenie do zadania-rodzica (`oneLevelUpButton`).        
        
        * Sortowanie widocznych zadań.
        
        * Zmiana trybu sortowania widocznych zadań.        
        
        * Automatyczne sortowanie TaskWidgetów w `taskExplorerVLayout` i ModelTasków w `chosenTaskLeft.subtasks` wg `chosenTaskLeft.sortMode` (może da się najpierw posortować w modelu, a później tylko odtworzyć listę TaskWidgetów z niego?) po dodaniu widgeta, usunięciu widgeta i przy inicjalizacji taskExplorera.        
        
        * Aktualizacja kalendarza po zmianie wyświetlanego tygodnia, na podstawie `chosenWeek` (który to zresztą `chosenWeek` powinien być zmieniany po wciśnięciu nextWeek albo previousWeek).        
        
        * Dodanie timeblocka.
        
        * Zapewnienie, że dwa timeblocki na siebie nie zachodzą. Sprawdzając w liście timeblocków, czy podany startTime mieści się między startTime i endTime jakiegokolwiek bloku.        
        
        * Edycja timeblocka.
        
        * Usunięcie timeblocka.      
        
        * Podświetlenie klikniętego LPM (pewnie to samo, co aktywnego) zadania.
        
        * Podświetlenie klikniętego LPM (j.w.) bloku czasu w kalendarzu.
        
        * Podświetlenie klikniętego LPM (j.w.) bloku czasu w szczegółach zadania.
        
        * Podświetlenie bieżącego dnia w kalendarzu.
        
        * Zapis stanu modelu (listy tasków i listy timeblocków) do bazy (Ctrl + S).
        
* `model.py`:

    * Konstruktory:
    
        * ModelTask:
        
            * Dodać pole `sortMode` (jeszcze nie wiem jakiego typu wartości będzie przechowywać, najlepiej enum (o ile da się to łatwo zapisać i odtworzyć z bazy), ale jeszcze nie wiem jak to zrobić w Pythonie)
    
    * Dodatkowe klasy albo funkcje:
    
        * Serializacja do bazy
        
        * Deserializacja z bazy

* `TaskWidget`:

    * Konstruktor:
    
        * Przekazywanie do niego obiektu typu `ModelTask`, który ma być przypisany jako `modelTask` w środku TaskWidgeta. Jeśli dopiero tworzymy zadanie, to w slocie mainWindow przekazujemy do konstruktora `TaskWidget` wywołanie konstruktora `ModelTask` z odpowiednimi argumentami.
    
        * Ustawienie `nrOfSubtasks` w oparciu o długość `modelTask.subtasks`.
    
    * Sloty:

* `calendarTimeblock`:

    * Konstruktor:
    
        * Przekazywanie do niego obiektu typu `ModelTimeblock`, który ma być przypisany jako `modelTimeblock` w środku CalendarTimeblocka. Zawsze musi już istnieć, więc w slocie w mainWindow nie można przekazać do konstruktora tej klasy wywołania konstruktora `ModelTimeblock`.
    
    * Sloty:
    
* `taskTimeblock`:

    * Konstruktor:
    
        * Przekazywanie do niego obiektu typu `ModelTimeblock`, który ma być przypisany jako `modelTimeblock` w środku CalendarTimeblocka. Jeśli dopiero tworzymy zadanie, to w slocie mainWindow przekazujemy do konstruktora `TaskTimeblock` wywołanie konstruktora `ModelTimeblock` z odpowiednimi argumentami.
    
    * Sloty:

* `addTaskDialog` / `editTaskDialog` ORAZ `addTimeblockDialog` / `editTimeblockDialog`:

    * Konstruktor:
    
        * Podawanie obecnego czasu (addDialog) albo dotychczasowego stanu zmienianych właśnie wartości (editDialog)
    
    * Sloty:
    
* `copyTaskDialog` / `moveTaskDialog`:

    * Konstruktor:
    
        * Dodanie pola `self.chosenTask`
        
        * Sprawienie, że `sortButton` wyświetla menu (skopiować kod robiący to z `mainWindow`).
    
    * Sloty:
    
        * Sprawienie, aby taski w tych dialogach nie wyświetlały menu kontekstowego. To pewnie trzeba zrobić w stylu `task1.setContextMenuPolicy(PreventContextMenu)` dla każdego taska, który trafia do tutejszego `taskExplorerVLayout`.
        
        * Wszystko, co będzie pasować z mainWindow (ten sam kod slotów do taskExplorera) skopiować tutaj.

    <br>

## Ważne info

* "It is possible to enable passing any Python object as a signal argument by specifying `PyQt_PyObject` as the type of argument in its signature. For example:

  ```python
  finished = pyqtSignal('PyQt_PyObject')
  ```

  This would normally be used for passing objects where the actual Python type isn’t known."

  <br>

  ---

  <br>

* "Although PyQt5 allows any Python method to be used as a slot when connecting signals, it is sometimes necessary to explicitly mark a method as being a Qt slot and to provide a C++ signature for it. PyQt5 provides the `pyqtSlot()` decorator to do this.

  ```
  @pyqtSlot(type1, type2..., result)
  ```

  Parameters:

    * `types` – the types that define the C++ signature of the slot. Each type may be a Python type or a string that is the name of a C++ type.

    * `result` – the type of the result. May be a Python type or a string that specifies a C++ type. This may only be given as a keyword argument (=...).

  Examples:

  > ```python
  > from PyQt5.QtCore import QObject, pyqtSlot
  >    
  >     class Foo(QObject):
  >    
  >        @pyqtSlot()
  >        def foo(self):
  >            # C++: void foo()
  > 
  >        @pyqtSlot(int, str)
  >        def foo(self, arg1, arg2):
  >            # C++: void foo(int, QString)
  > 
  >        @pyqtSlot(int, name='bar')
  >        def foo(self, arg1):
  >            # C++: void bar(int)
  > 
  >        @pyqtSlot(int, result=int)
  >        def foo(self, arg1):
  >            # C++: int foo(int)
  > 
  >        @pyqtSlot(int, QObject)
  >        def foo(self, arg1):
  >            # C++: int foo(int, QObject *)
  > ```

  ---

  <br>

* Dialog wywołuje się tak, że najpierw tworzy się jego instancję, a następnie robi `dialog.exec_()`.

<br>

## Przydatne linki

* [Dokumentacja PySide](https://doc.qt.io/qtforpython-5/modules.html) (praktycznie to samo co PyQt, jedynie mniej popularne i utrzymywane przez samo Qt Project).

* [Widget Styling](https://doc.qt.io/qtforpython-5/tutorials/basictutorial/widgetstyling.html) (też z PySide'a).

* Dokumentacja PyQt (Tylko do podpatrzenia, jaka jest nazwa danej klasy, metody, czy pola, bo nie zawsze są takie same jak w Qt albo PySide):

    * [Qt Core](https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtcore/qtcore-module.html)

    * [Qt Widgets](https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtwidgets/qtwidgets-module.html#)

    * [QtGui](https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtgui/qtgui-module.html)