from PyQt5.QtGui import QFont
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QFrame, QSizePolicy, QVBoxLayout, QLabel, QApplication


class CalendarTimeblock(QFrame):
    def __init__(self, modelTimeblock_, parent = None):
        super().__init__(parent)

        self.modelTimeblock = modelTimeblock_

        self.setObjectName("calendarTimeblock")
        self.resize(94, 47)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QSize(87, 0))
        self.setFrameShape(QFrame.StyledPanel)
        self.setFrameShadow(QFrame.Raised)
        self.calendarTimeblockVLayout = QVBoxLayout(self)
        self.calendarTimeblockVLayout.setContentsMargins(0, 0, 0, 0)
        self.calendarTimeblockVLayout.setSpacing(0)
        self.calendarTimeblockVLayout.setObjectName("calendarTimeblockVLayout")

        self.start = QLabel(self)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.start.sizePolicy().hasHeightForWidth())
        self.start.setSizePolicy(sizePolicy)
        self.start.setMinimumSize(QSize(87, 0))
        self.start.setMaximumSize(QSize(16777215, 15))
        self.start.setFrameShape(QFrame.NoFrame)
        self.start.setFrameShadow(QFrame.Raised)
        self.start.setLineWidth(1)
        self.start.setMidLineWidth(0)
        self.start.setTextFormat(Qt.PlainText)
        self.start.setAlignment(Qt.AlignCenter)
        self.start.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.start.setObjectName("start")
        self.calendarTimeblockVLayout.addWidget(self.start)

        self.taskName = QLabel(self)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.taskName.sizePolicy().hasHeightForWidth())
        self.taskName.setSizePolicy(sizePolicy)
        self.taskName.setMinimumSize(QSize(0, 15))
        self.taskName.setLayoutDirection(Qt.LeftToRight)
        self.taskName.setFrameShape(QFrame.StyledPanel)
        self.taskName.setFrameShadow(QFrame.Plain)
        self.taskName.setLineWidth(1)
        self.taskName.setMidLineWidth(0)
        self.taskName.setTextFormat(Qt.PlainText)
        self.taskName.setAlignment(Qt.AlignCenter)
        self.taskName.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.taskName.setObjectName("taskName")
        self.calendarTimeblockVLayout.addWidget(self.taskName)

        self.end = QLabel(self)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.end.sizePolicy().hasHeightForWidth())
        self.end.setSizePolicy(sizePolicy)
        self.end.setMinimumSize(QSize(87, 0))
        self.end.setMaximumSize(QSize(16777215, 15))
        font = QFont()
        font.setFamily("Ubuntu")
        font.setPointSize(10)
        font.setItalic(False)
        self.end.setFont(font)
        self.end.setFrameShape(QFrame.NoFrame)
        self.end.setFrameShadow(QFrame.Raised)
        self.end.setLineWidth(1)
        self.end.setMidLineWidth(0)
        self.end.setTextFormat(Qt.PlainText)
        self.end.setAlignment(Qt.AlignCenter)
        self.end.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.end.setObjectName("end")
        self.calendarTimeblockVLayout.addWidget(self.end)

        self.taskName.setText("to_automate") # self.modelTimeblock.modelTask.name
        self.start.setText("to_automate") # self.modelTimeblock.start
        self.end.setText("to_automate") # self.modelTimeblock.end

"""
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    calTimeblock = CalendarTimeblock(None)
    calTimeblock.show()

    sys.exit(app.exec_())
"""