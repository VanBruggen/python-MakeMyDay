
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QDialog, QSpacerItem, QDialogButtonBox, QDateEdit, QTimeEdit, QSizePolicy, QGridLayout, QLabel, QApplication


# TODO:
# 1. Przekazywanie wejściowych wartości dla input widgetów jako argumenty konstruktora (obecny dzień?)


class AddTimeblockDialog(QDialog):
    def __init__(self, parent = None):
        super().__init__(parent)
        
        self.setObjectName("addTimeblockDialog")
        self.setWindowModality(Qt.ApplicationModal)
        self.resize(293, 266)
        self.setMinimumSize(QSize(293, 266))
        self.setMaximumSize(QSize(293, 266))
        self.setModal(True)
        self.addTimeblockDialogGLayout = QGridLayout(self)
        self.addTimeblockDialogGLayout.setContentsMargins(40, 10, 40, 10)
        self.addTimeblockDialogGLayout.setHorizontalSpacing(30)
        self.addTimeblockDialogGLayout.setVerticalSpacing(10)
        self.addTimeblockDialogGLayout.setObjectName("addTimeblockDialogGLayout")
        
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self.addTimeblockDialogGLayout.addWidget(self.buttonBox, 4, 0, 1, 2)
        
        self.startLabel = QLabel(self)
        self.startLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.startLabel.setObjectName("startLabel")       
        self.addTimeblockDialogGLayout.addWidget(self.startLabel, 1, 0, 1, 1)
       
        self.start = QTimeEdit(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.start.sizePolicy().hasHeightForWidth())
        self.start.setSizePolicy(sizePolicy)
        self.start.setMinimumSize(QSize(70, 0))
        self.start.setMaximumSize(QSize(70, 16777215))
        self.start.setAlignment(Qt.AlignCenter)
        self.start.setCalendarPopup(True)
        self.start.setObjectName("start")
        self.addTimeblockDialogGLayout.addWidget(self.start, 1, 1, 1, 1)
        
        self.end = QTimeEdit(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.end.sizePolicy().hasHeightForWidth())
        self.end.setSizePolicy(sizePolicy)
        self.end.setMinimumSize(QSize(70, 0))
        self.end.setMaximumSize(QSize(70, 16777215))
        self.end.setAlignment(Qt.AlignCenter)
        self.end.setCalendarPopup(True)
        self.end.setObjectName("end")        
        self.addTimeblockDialogGLayout.addWidget(self.end, 2, 1, 1, 1)
        
        self.endLabel = QLabel(self)
        self.endLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.endLabel.setObjectName("endLabel")
        self.addTimeblockDialogGLayout.addWidget(self.endLabel, 2, 0, 1, 1)
        
        self.date = QDateEdit(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.date.sizePolicy().hasHeightForWidth())
        self.date.setSizePolicy(sizePolicy)
        self.date.setMinimumSize(QSize(120, 0))
        self.date.setMaximumSize(QSize(120, 16777215))
        self.date.setAlignment(Qt.AlignCenter)
        self.date.setCalendarPopup(True)
        self.date.setObjectName("date")
        self.addTimeblockDialogGLayout.addWidget(self.date, 0, 1, 1, 1)
        
        self.dateLabel = QLabel(self)
        self.dateLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.dateLabel.setObjectName("dateLabel")
        self.addTimeblockDialogGLayout.addWidget(self.dateLabel, 0, 0, 1, 1)
        spacerItem = QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.addTimeblockDialogGLayout.addItem(spacerItem, 3, 1, 1, 1)
        
        self.startLabel.setBuddy(self.start)
        self.endLabel.setBuddy(self.end)
        self.dateLabel.setBuddy(self.date)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.setTabOrder(self.date, self.start)
        self.setTabOrder(self.start, self.end)

        self.setWindowTitle( "Add a new timeblock...")
        self.startLabel.setText("Start time:")
        self.endLabel.setText("End time:")
        self.dateLabel.setText("Date:")

    def getAll(self):
        d = self.date.text()
        s = self.start.text()
        e = self.end.text()
        return d, s, e

"""
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    temp = AddTimeblockDialog()
    temp.show()

    sys.exit(app.exec_())
"""
