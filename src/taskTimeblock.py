from PyQt5.QtGui import QFont, QIcon, QPixmap
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QFrame, QToolButton, QSizePolicy, QGridLayout, QLabel, QApplication

import resources_rc


class TaskTimeblock(QFrame):
    def __init__(self, modelTask_, modelTimeblock_, date_ = "28.11.2021", startTime_ = "18:00", endTime_ = "20:00", parent = None):
        super().__init__(parent)

        self.modelTask = modelTask_
        self.modelTimeblock = modelTimeblock_

        self.resize(157, 73)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QSize(157, 73))
        self.setMaximumSize(QSize(157, 73))
        self.setFrameShape(QFrame.StyledPanel)
        self.setFrameShadow(QFrame.Plain)
        self.setObjectName("taskTimeblock")
        self.taskTimeblockGLayout = QGridLayout(self)
        self.taskTimeblockGLayout.setContentsMargins(5, 5, 5, 5)
        self.taskTimeblockGLayout.setSpacing(5)
        self.taskTimeblockGLayout.setObjectName("taskTimeblockGLayout")

        self.deleteButton = QToolButton(self)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/icons/icons/delete-bin-7-line.svg"), QIcon.Normal, QIcon.Off)
        self.deleteButton.setIcon(icon)
        self.deleteButton.setIconSize(QSize(20, 20))
        self.deleteButton.setObjectName("deleteButton")
        self.taskTimeblockGLayout.addWidget(self.deleteButton, 0, 0, 1, 1)

        self.date = QLabel(self)
        font = QFont()
        font.setPointSize(10)
        self.date.setFont(font)
        self.date.setFrameShape(QFrame.NoFrame)
        self.date.setTextFormat(Qt.PlainText)
        self.date.setAlignment(Qt.AlignCenter)
        self.date.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.date.setObjectName("date")
        self.taskTimeblockGLayout.addWidget(self.date, 0, 1, 1, 2)

        self.editButton = QToolButton(self)
        icon1 = QIcon()
        icon1.addPixmap(QPixmap(":/icons/icons/edit-box-line.svg"), QIcon.Normal, QIcon.Off)
        self.editButton.setIcon(icon1)
        self.editButton.setIconSize(QSize(20, 20))
        self.editButton.setObjectName("editButton")
        self.taskTimeblockGLayout.addWidget(self.editButton, 0, 3, 1, 1)

        self.startTime = QLabel(self)
        font = QFont()
        font.setPointSize(8)
        self.startTime.setFont(font)
        self.startTime.setFrameShape(QFrame.StyledPanel)
        self.startTime.setTextFormat(Qt.PlainText)
        self.startTime.setAlignment(Qt.AlignCenter)
        self.startTime.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.startTime.setObjectName("startTime")
        self.taskTimeblockGLayout.addWidget(self.startTime, 1, 0, 1, 2)

        self.endTime = QLabel(self)
        font = QFont()
        font.setPointSize(8)
        self.endTime.setFont(font)
        self.endTime.setFrameShape(QFrame.StyledPanel)
        self.endTime.setTextFormat(Qt.PlainText)
        self.endTime.setAlignment(Qt.AlignCenter)
        self.endTime.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.endTime.setObjectName("endTime")
        self.taskTimeblockGLayout.addWidget(self.endTime, 1, 2, 1, 2)

        self.deleteButton.setText("...")
        self.date.setText(date_)
        self.editButton.setText("...")
        self.startTime.setText(startTime_)
        self.endTime.setText(endTime_)

"""
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    taskTimeblock = TaskTimeblock(None)
    taskTimeblock.show()

    sys.exit(app.exec_())
"""