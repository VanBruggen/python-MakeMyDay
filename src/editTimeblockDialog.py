from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QDialog, QSpacerItem, QDialogButtonBox, QDateEdit, QTimeEdit, QSizePolicy, QGridLayout, QLabel, QApplication


# TODO:
# 1. Przekazywanie wejściowych wartości dla input widgetów jako argumenty konstruktora (obecnie ustawione wartości)


class EditTimeblockDialog(QDialog):
    def __init__(self, parent = None):
        super().__init__(parent)

        self.setObjectName("editTimeblockDialog")
        self.setWindowModality(Qt.ApplicationModal)
        self.resize(293, 266)
        self.setMinimumSize(QSize(293, 266))
        self.setMaximumSize(QSize(293, 266))
        self.setModal(True)
        self.editTimeblockDialogGLayout = QGridLayout(self)
        self.editTimeblockDialogGLayout.setContentsMargins(40, 10, 40, 10)
        self.editTimeblockDialogGLayout.setHorizontalSpacing(30)
        self.editTimeblockDialogGLayout.setVerticalSpacing(10)
        self.editTimeblockDialogGLayout.setObjectName("editTimeblockDialogGLayout")

        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self.editTimeblockDialogGLayout.addWidget(self.buttonBox, 4, 0, 1, 2)

        self.startLabel = QLabel(self)
        self.startLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.startLabel.setObjectName("startLabel")
        self.editTimeblockDialogGLayout.addWidget(self.startLabel, 1, 0, 1, 1)

        self.start = QTimeEdit(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.start.sizePolicy().hasHeightForWidth())
        self.start.setSizePolicy(sizePolicy)
        self.start.setMinimumSize(QSize(70, 0))
        self.start.setMaximumSize(QSize(70, 16777215))
        self.start.setAlignment(Qt.AlignCenter)
        self.start.setCalendarPopup(True)
        self.start.setObjectName("start")
        self.editTimeblockDialogGLayout.addWidget(self.start, 1, 1, 1, 1)

        self.end = QTimeEdit(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.end.sizePolicy().hasHeightForWidth())
        self.end.setSizePolicy(sizePolicy)
        self.end.setMinimumSize(QSize(70, 0))
        self.end.setMaximumSize(QSize(70, 16777215))
        self.end.setAlignment(Qt.AlignCenter)
        self.end.setCalendarPopup(True)
        self.end.setObjectName("end")
        self.editTimeblockDialogGLayout.addWidget(self.end, 2, 1, 1, 1)

        self.endLabel = QLabel(self)
        self.endLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.endLabel.setObjectName("endLabel")
        self.editTimeblockDialogGLayout.addWidget(self.endLabel, 2, 0, 1, 1)

        self.date = QDateEdit(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.date.sizePolicy().hasHeightForWidth())
        self.date.setSizePolicy(sizePolicy)
        self.date.setMinimumSize(QSize(120, 0))
        self.date.setMaximumSize(QSize(120, 16777215))
        self.date.setAlignment(Qt.AlignCenter)
        self.date.setCalendarPopup(True)
        self.date.setObjectName("date")
        self.editTimeblockDialogGLayout.addWidget(self.date, 0, 1, 1, 1)

        self.dateLabel = QLabel(self)
        self.dateLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.dateLabel.setObjectName("dateLabel")
        self.editTimeblockDialogGLayout.addWidget(self.dateLabel, 0, 0, 1, 1)

        spacerItem = QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.editTimeblockDialogGLayout.addItem(spacerItem, 3, 1, 1, 1)

        self.startLabel.setBuddy(self.start)
        self.endLabel.setBuddy(self.end)
        self.dateLabel.setBuddy(self.date)

        self.buttonBox.accepted.connect(self.accept) # type: ignore
        self.buttonBox.rejected.connect(self.reject) # type: ignore

        self.setTabOrder(self.date, self.start)
        self.setTabOrder(self.start, self.end)

        self.setWindowTitle("Edit timeblock...")
        self.startLabel.setText("Start time:")
        self.endLabel.setText("End time:")
        self.dateLabel.setText("Date:")

"""
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    editTimeblockDialog = EditTimeblockDialog()
    editTimeblockDialog.show()

    sys.exit(app.exec_())
"""