from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QSize, Qt, QRegExp
from PyQt5.QtWidgets import QDialog, QPlainTextEdit, QAbstractScrollArea, QDialogButtonBox, QSpinBox, QLineEdit, QDateTimeEdit, QSizePolicy, QFormLayout, QLabel, QApplication


# TODO:
# 1. Przekazywanie wejściowych wartości dla input widgetów jako argumenty konstruktora (obecny dzień?)


class AddTaskDialog(QDialog):
    def __init__(self, parent = None):
        super().__init__(parent)

        self.setObjectName("addTaskDialog")
        self.setWindowModality(Qt.ApplicationModal)
        self.resize(529, 418)
        self.setMinimumSize(QSize(529, 418))
        self.setSizeGripEnabled(False)
        self.setModal(True)
        self.addTaskDialogFormLayout = QFormLayout(self)
        self.addTaskDialogFormLayout.setFormAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.addTaskDialogFormLayout.setContentsMargins(20, 20, 20, 20)
        self.addTaskDialogFormLayout.setSpacing(20)
        self.addTaskDialogFormLayout.setObjectName("addTaskDialogFormLayout")

        self.taskNameLabel = QLabel(self)
        self.taskNameLabel.setObjectName("taskNameLabel")
        self.addTaskDialogFormLayout.setWidget(0, QFormLayout.LabelRole, self.taskNameLabel)

        self.taskName = QLineEdit(self)
        self.taskName.setClearButtonEnabled(True)
        self.taskName.setObjectName("taskName")
        self.addTaskDialogFormLayout.setWidget(0, QFormLayout.FieldRole, self.taskName)

        self.deadlineLabel = QLabel(self)
        self.deadlineLabel.setObjectName("deadlineLabel")
        self.addTaskDialogFormLayout.setWidget(1, QFormLayout.LabelRole, self.deadlineLabel)

        self.deadline = QDateTimeEdit(self)
        self.deadline.setAlignment(Qt.AlignCenter)
        self.deadline.setReadOnly(False)
        self.deadline.setProperty("showGroupSeparator", False)
        self.deadline.setCalendarPopup(True)
        self.deadline.setObjectName("deadline")
        self.addTaskDialogFormLayout.setWidget(1, QFormLayout.FieldRole, self.deadline)

        self.priorityLabel = QLabel(self)
        self.priorityLabel.setObjectName("priorityLabel")
        self.addTaskDialogFormLayout.setWidget(2, QFormLayout.LabelRole, self.priorityLabel)

        self.priority = QSpinBox(self)
        self.priority.setAlignment(Qt.AlignCenter)
        self.priority.setSpecialValueText("")
        self.priority.setMinimum(1)
        self.priority.setMaximum(5)
        self.priority.setObjectName("priority")
        self.addTaskDialogFormLayout.setWidget(2, QFormLayout.FieldRole, self.priority)

        self.descriptionLabel = QLabel(self)
        self.descriptionLabel.setObjectName("descriptionLabel")
        self.addTaskDialogFormLayout.setWidget(3, QFormLayout.LabelRole, self.descriptionLabel)

        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.addTaskDialogFormLayout.setWidget(4, QFormLayout.SpanningRole, self.buttonBox)

        self.description = QPlainTextEdit(self)
        self.description.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.description.setSizeAdjustPolicy(QAbstractScrollArea.AdjustIgnored)
        self.description.setReadOnly(False)
        self.description.setPlainText("")
        self.description.setOverwriteMode(False)
        self.description.setBackgroundVisible(False)
        self.description.setObjectName("description")
        self.addTaskDialogFormLayout.setWidget(3, QFormLayout.FieldRole, self.description)

        self.taskNameLabel.setBuddy(self.taskName)
        self.deadlineLabel.setBuddy(self.deadline)
        self.priorityLabel.setBuddy(self.priority)

        self.buttonBox.accepted.connect(self.accept) # type: ignore
        self.buttonBox.rejected.connect(self.reject) # type: ignore

        self.setWindowTitle("Add a new task...")
        self.taskNameLabel.setText("Name:")
        self.deadlineLabel.setText("Deadline:")
        self.priorityLabel.setText("Priority:")
        self.descriptionLabel.setText("Description:")

        validator = QRegExpValidator(QRegExp(r'[\w ]{1,32}'))
        self.taskName.setValidator(validator)

    def getAll(self):
        tn = self.taskName.text()
        d = self.deadline.text()
        p = self.priority.text()
        ds = self.description.toPlainText()
        return tn, d, p, ds

"""
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    addTaskDialog = AddTaskDialog()
    addTaskDialog.show()

    sys.exit(app.exec_())
"""
