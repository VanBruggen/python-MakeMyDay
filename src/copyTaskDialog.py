from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import QSize, Qt, QRect
from PyQt5.QtWidgets import QDialog, QFrame, QWidget, QSpacerItem, QScrollArea, QToolButton, QSizePolicy, QVBoxLayout, QHBoxLayout, QLabel, QApplication

import resources_rc


# TODO:
# 1. 


class CopyTaskDialog(QDialog):
    def __init__(self, parent = None):
        super().__init__(parent)
        
        self.setObjectName("copyTaskDialog")
        self.setWindowModality(Qt.ApplicationModal)
        self.resize(797, 489)
        self.setMinimumSize(QSize(797, 489))
        self.setModal(True)
        self.copyTaskDialogVLayout = QVBoxLayout(self)
        self.copyTaskDialogVLayout.setContentsMargins(10, 10, 10, 10)
        self.copyTaskDialogVLayout.setSpacing(0)
        self.copyTaskDialogVLayout.setObjectName("copyTaskDialogVLayout")

        self.taskExplorerFrame = QFrame(self)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.taskExplorerFrame.sizePolicy().hasHeightForWidth())
        self.taskExplorerFrame.setSizePolicy(sizePolicy)
        self.taskExplorerFrame.setMinimumSize(QSize(0, 250))
        self.taskExplorerFrame.setFrameShape(QFrame.Panel)
        self.taskExplorerFrame.setFrameShadow(QFrame.Plain)
        self.taskExplorerFrame.setLineWidth(0)
        self.taskExplorerFrame.setObjectName("taskExplorerFrame")
        self.taskExplorerFrameVLayout = QVBoxLayout(self.taskExplorerFrame)
        self.taskExplorerFrameVLayout.setContentsMargins(0, 0, 0, 0)
        self.taskExplorerFrameVLayout.setSpacing(5)
        self.taskExplorerFrameVLayout.setObjectName("taskExplorerFrameVLayout")

        self.taskExplorerPathWidget = QWidget(self.taskExplorerFrame)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.taskExplorerPathWidget.sizePolicy().hasHeightForWidth())
        self.taskExplorerPathWidget.setSizePolicy(sizePolicy)
        self.taskExplorerPathWidget.setMinimumSize(QSize(0, 40))
        self.taskExplorerPathWidget.setMaximumSize(QSize(16777215, 40))
        self.taskExplorerPathWidget.setObjectName("taskExplorerPathWidget")
        self.taskExplorerPathHLayout = QHBoxLayout(self.taskExplorerPathWidget)
        self.taskExplorerPathHLayout.setContentsMargins(0, 0, 0, 0)
        self.taskExplorerPathHLayout.setSpacing(5)
        self.taskExplorerPathHLayout.setObjectName("taskExplorerPathHLayout")

        self.taskExplorerPath = QLabel(self.taskExplorerPathWidget)
        self.taskExplorerPath.setFrameShape(QFrame.StyledPanel)
        self.taskExplorerPath.setTextFormat(Qt.PlainText)
        self.taskExplorerPath.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.taskExplorerPath.setObjectName("taskExplorerPath")
        self.taskExplorerPathHLayout.addWidget(self.taskExplorerPath)

        self.oneLevelUpButton = QToolButton(self.taskExplorerPathWidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.oneLevelUpButton.sizePolicy().hasHeightForWidth())
        self.oneLevelUpButton.setSizePolicy(sizePolicy)
        self.oneLevelUpButton.setMinimumSize(QSize(36, 36))
        self.oneLevelUpButton.setMaximumSize(QSize(36, 36))
        icon = QIcon()
        icon.addPixmap(QPixmap(":/icons/icons/arrow-up-line.svg"), QIcon.Normal, QIcon.Off)
        self.oneLevelUpButton.setIcon(icon)
        self.oneLevelUpButton.setIconSize(QSize(28, 28))
        self.oneLevelUpButton.setObjectName("oneLevelUpButton")
        self.taskExplorerPathHLayout.addWidget(self.oneLevelUpButton)
        self.taskExplorerFrameVLayout.addWidget(self.taskExplorerPathWidget)

        self.taskHeadersFrame = QFrame(self.taskExplorerFrame)
        self.taskHeadersFrame.setFrameShape(QFrame.StyledPanel)
        self.taskHeadersFrame.setFrameShadow(QFrame.Raised)
        self.taskHeadersFrame.setObjectName("taskHeadersFrame")
        self.taskExplorerHeadersHLayout = QHBoxLayout(self.taskHeadersFrame)
        self.taskExplorerHeadersHLayout.setContentsMargins(5, 5, 5, 5)
        self.taskExplorerHeadersHLayout.setSpacing(10)
        self.taskExplorerHeadersHLayout.setObjectName("taskExplorerHeadersHLayout")
        spacerItem = QSpacerItem(33, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)
        self.taskExplorerHeadersHLayout.addItem(spacerItem)
        self.headersTaskName = QLabel(self.taskHeadersFrame)
        self.headersTaskName.setTextFormat(Qt.PlainText)
        self.headersTaskName.setTextInteractionFlags(Qt.NoTextInteraction)
        self.headersTaskName.setObjectName("headersTaskName")
        self.taskExplorerHeadersHLayout.addWidget(self.headersTaskName)

        self.headersDeadline = QLabel(self.taskHeadersFrame)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.headersDeadline.sizePolicy().hasHeightForWidth())
        self.headersDeadline.setSizePolicy(sizePolicy)
        self.headersDeadline.setMinimumSize(QSize(110, 0))
        self.headersDeadline.setMaximumSize(QSize(104, 16777215))
        self.headersDeadline.setTextFormat(Qt.PlainText)
        self.headersDeadline.setAlignment(Qt.AlignCenter)
        self.headersDeadline.setTextInteractionFlags(Qt.NoTextInteraction)
        self.headersDeadline.setObjectName("headersDeadline")
        self.taskExplorerHeadersHLayout.addWidget(self.headersDeadline)

        self.headersPriority = QLabel(self.taskHeadersFrame)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.headersPriority.sizePolicy().hasHeightForWidth())
        self.headersPriority.setSizePolicy(sizePolicy)
        self.headersPriority.setTextFormat(Qt.PlainText)
        self.headersPriority.setAlignment(Qt.AlignCenter)
        self.headersPriority.setTextInteractionFlags(Qt.NoTextInteraction)
        self.headersPriority.setObjectName("headersPriority")
        self.taskExplorerHeadersHLayout.addWidget(self.headersPriority)

        spacerItem1 = QSpacerItem(10, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)
        self.taskExplorerHeadersHLayout.addItem(spacerItem1)

        self.headersNrOfSubtasks = QLabel(self.taskHeadersFrame)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.headersNrOfSubtasks.sizePolicy().hasHeightForWidth())
        self.headersNrOfSubtasks.setSizePolicy(sizePolicy)
        self.headersNrOfSubtasks.setTextFormat(Qt.PlainText)
        self.headersNrOfSubtasks.setAlignment(Qt.AlignCenter)
        self.headersNrOfSubtasks.setTextInteractionFlags(Qt.NoTextInteraction)
        self.headersNrOfSubtasks.setObjectName("headersNrOfSubtasks")
        self.taskExplorerHeadersHLayout.addWidget(self.headersNrOfSubtasks)

        spacerItem2 = QSpacerItem(76, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)
        self.taskExplorerHeadersHLayout.addItem(spacerItem2)

        self.taskExplorerFrameVLayout.addWidget(self.taskHeadersFrame)

        self.taskExplorerScrollArea = QScrollArea(self.taskExplorerFrame)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(6)
        sizePolicy.setHeightForWidth(self.taskExplorerScrollArea.sizePolicy().hasHeightForWidth())
        self.taskExplorerScrollArea.setSizePolicy(sizePolicy)
        self.taskExplorerScrollArea.setFrameShape(QFrame.NoFrame)
        self.taskExplorerScrollArea.setFrameShadow(QFrame.Plain)
        self.taskExplorerScrollArea.setLineWidth(1)
        self.taskExplorerScrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.taskExplorerScrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.taskExplorerScrollArea.setWidgetResizable(True)
        self.taskExplorerScrollArea.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.taskExplorerScrollArea.setObjectName("taskExplorerScrollArea")

        self.taskExplorer = QWidget()
        self.taskExplorer.setGeometry(QRect(0, 0, 764, 322))
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.taskExplorer.sizePolicy().hasHeightForWidth())
        self.taskExplorer.setSizePolicy(sizePolicy)
        self.taskExplorer.setObjectName("taskExplorer")
        self.taskExplorerVLayout = QVBoxLayout(self.taskExplorer)
        self.taskExplorerVLayout.setContentsMargins(10, 0, 10, 5)
        self.taskExplorerVLayout.setSpacing(5)
        self.taskExplorerVLayout.setObjectName("taskExplorerVLayout")

        spacerItem3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.taskExplorerVLayout.addItem(spacerItem3)
        self.taskExplorerScrollArea.setWidget(self.taskExplorer)
        self.taskExplorerFrameVLayout.addWidget(self.taskExplorerScrollArea)

        self.taskExplorerToolbar = QFrame(self.taskExplorerFrame)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.taskExplorerToolbar.sizePolicy().hasHeightForWidth())
        self.taskExplorerToolbar.setSizePolicy(sizePolicy)
        self.taskExplorerToolbar.setMinimumSize(QSize(0, 60))
        self.taskExplorerToolbar.setMaximumSize(QSize(16777215, 60))
        self.taskExplorerToolbar.setFrameShape(QFrame.StyledPanel)
        self.taskExplorerToolbar.setFrameShadow(QFrame.Plain)
        self.taskExplorerToolbar.setLineWidth(1)
        self.taskExplorerToolbar.setObjectName("taskExplorerToolbar")

        self.taskExplorerToolbarHLayout = QHBoxLayout(self.taskExplorerToolbar)
        self.taskExplorerToolbarHLayout.setContentsMargins(5, 0, 5, 0)
        self.taskExplorerToolbarHLayout.setSpacing(5)
        self.taskExplorerToolbarHLayout.setObjectName("taskExplorerToolbarHLayout")

        self.cancelButton = QToolButton(self.taskExplorerToolbar)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cancelButton.sizePolicy().hasHeightForWidth())
        self.cancelButton.setSizePolicy(sizePolicy)
        self.cancelButton.setMinimumSize(QSize(0, 50))
        self.cancelButton.setMaximumSize(QSize(16777215, 50))
        icon1 = QIcon()
        icon1.addPixmap(QPixmap(":/icons/icons/prohibit.svg"), QIcon.Normal, QIcon.Off)
        self.cancelButton.setIcon(icon1)
        self.cancelButton.setIconSize(QSize(24, 24))
        self.cancelButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.cancelButton.setObjectName("cancelButton")
        self.taskExplorerToolbarHLayout.addWidget(self.cancelButton)

        self.sortButton = QToolButton(self.taskExplorerToolbar)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sortButton.sizePolicy().hasHeightForWidth())
        self.sortButton.setSizePolicy(sizePolicy)
        self.sortButton.setMinimumSize(QSize(0, 50))
        self.sortButton.setMaximumSize(QSize(16777215, 50))
        icon2 = QIcon()
        icon2.addPixmap(QPixmap(":/icons/icons/sort-desc.svg"), QIcon.Normal, QIcon.Off)
        self.sortButton.setIcon(icon2)
        self.sortButton.setIconSize(QSize(24, 24))
        self.sortButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.sortButton.setObjectName("sortButton")
        self.taskExplorerToolbarHLayout.addWidget(self.sortButton)

        self.moveButton = QToolButton(self.taskExplorerToolbar)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.moveButton.sizePolicy().hasHeightForWidth())
        self.moveButton.setSizePolicy(sizePolicy)
        self.moveButton.setMinimumSize(QSize(0, 50))
        self.moveButton.setMaximumSize(QSize(16777215, 50))
        icon3 = QIcon()
        icon3.addPixmap(QPixmap(":/icons/icons/file-copy-line.svg"), QIcon.Normal, QIcon.Off)
        self.moveButton.setIcon(icon3)
        self.moveButton.setIconSize(QSize(24, 24))
        self.moveButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.moveButton.setObjectName("moveButton")
        
        self.taskExplorerToolbarHLayout.addWidget(self.moveButton)
        self.taskExplorerFrameVLayout.addWidget(self.taskExplorerToolbar)
        self.copyTaskDialogVLayout.addWidget(self.taskExplorerFrame)

        self.setTabOrder(self.oneLevelUpButton, self.cancelButton)
        self.setTabOrder(self.cancelButton, self.sortButton)
        self.setTabOrder(self.sortButton, self.moveButton)
        self.setTabOrder(self.moveButton, self.taskExplorerScrollArea)

        self.cancelButton.clicked.connect(self.reject)
        #self.moveButton.clicked.connect(self.cos_do_przesuwania)
        #self.sortButton.clicked.connect(self.cos_do_sortowania)

        self.setWindowTitle("Choose a location to copy task(s) to...")
        self.taskExplorerPath.setText("/")
        self.oneLevelUpButton.setText("...")
        self.headersTaskName.setText("Name")
        self.headersDeadline.setText("Deadline")
        self.headersPriority.setText("Priority")
        self.headersNrOfSubtasks.setText("Subtasks")
        self.cancelButton.setText("&Cancel")
        self.sortButton.setText("&Sort")
        self.moveButton.setText("&Copy task(s) here")

"""
if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)

    copyTaskDialog = CopyTaskDialog()
    copyTaskDialog.show()

    sys.exit(app.exec_())
"""