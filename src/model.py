import sqlite3
from enum import IntEnum


class SortMode(IntEnum):
    PRIORITY = 1
    DEADLINE = 2
    ALPHABET_ASC = 3
    ALPHABET_DESC = 4


class ModelTask:
    def __init__(self,
                 id_,
                 parentID_,
                 parentModelTask_,
                 name_ = "New task",
                 deadline_ = None,
                 priority_ = 1,
                 description_ = None,
                 sortMode_ = 1):

        self.id = id_
        self.parentID = parentID_

        self.parentModelTask = parentModelTask_
        self.subtasks = []          # lista podzadań – również elementów typu "ModelTask".
        self.checkedSubtasks = []   # lista zaznaczonych podzadań (checked), do której trafia każde zaznaczone zadanie.
        self.timeblocks = []        # lista elementów typu "ModelTimeblock" – segmenty czasu, które trafiają do kalendarza.

        self.name = name_
        self.checked = False
        self.deadline = deadline_   # DateTime
        self.priority = priority_
        self.description = description_
        self.sortMode = SortMode(base = sortMode_)   # SortMode (w praktyce int)


class ModelTimeblock:
    def __init__(self, id_, taskID_, relatedModelTask_, date_, start_, end_):

        self.id = id_
        self.taskID = taskID_

        self.relatedModelTask = relatedModelTask_

        self.date = date_       # Date
        self.start = start_     # Time (bo z założenia ograniczamy się do jednego dnia)


if __name__ == '__main__':
    conn = sqlite3.connect('temp.db')
    c = conn.cursor()
    # c.execute('''CREATE TABLE TaskTable (
    #             id integer,
    #             parentId integer,
    #
    #             name text,
    #             checked integer,
    #             deadline text,
    #             priority integer,
    #             description text)''')
    #c.execute("INSERT INTO TaskTable VALUES (1, 2, 'nazwa', 1, 'jutro', 1, 'opis')")
    c.execute("SELECT * FROM TaskTable")
    print(c.fetchone())
    conn.commit()

    conn.close()
