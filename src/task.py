from PyQt5.QtGui import QFont, QIcon, QPixmap
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QFrame, QCheckBox, QToolButton, QSizePolicy, QVBoxLayout, QHBoxLayout, QLabel, QApplication

import resources_rc

# TODO
# 1. Konstrukcja modelTask i dodanie do parentTaska


class TaskWidget(QFrame):
    def __init__(self, parentModelTask, name_ = "Name", deadline_ = "01.01.2000, 12:00", priority_ = "1", parent = None):
        super().__init__(parent)

        self.modelTask = None # TODO: Konstrukcja tutejszego Taska i dodanie go do parentModelTask.subtasks

        self.resize(423, 50)
        self.setMinimumSize(QSize(0, 50))
        self.setMaximumSize(QSize(16777215, 50))
        self.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.setFrameShape(QFrame.StyledPanel)
        self.setFrameShadow(QFrame.Plain)
        self.setObjectName("taskWidget")
        self.taskWidgetHorizontalLayout = QHBoxLayout(self)
        self.taskWidgetHorizontalLayout.setContentsMargins(5, 5, 5, 5)
        self.taskWidgetHorizontalLayout.setSpacing(10)
        self.taskWidgetHorizontalLayout.setObjectName("taskWidgetHorizontalLayout")
        
        self.checkBox = QCheckBox(self)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox.sizePolicy().hasHeightForWidth())
        self.checkBox.setSizePolicy(sizePolicy)
        self.checkBox.setContextMenuPolicy(Qt.NoContextMenu)
        self.checkBox.setText("")
        self.checkBox.setIconSize(QSize(16, 16))
        self.checkBox.setObjectName("checkBox")
        self.taskWidgetHorizontalLayout.addWidget(self.checkBox)

        self.name = QLabel(self)
        self.name.setContextMenuPolicy(Qt.NoContextMenu)
        self.name.setText(name_)
        self.name.setTextFormat(Qt.PlainText)
        self.name.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.name.setObjectName("name")
        self.taskWidgetHorizontalLayout.addWidget(self.name)

        self.deadline = QLabel(self)
        self.deadline.setText(deadline_)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.deadline.sizePolicy().hasHeightForWidth())
        self.deadline.setSizePolicy(sizePolicy)
        self.deadline.setContextMenuPolicy(Qt.NoContextMenu)
        self.deadline.setTextFormat(Qt.PlainText)
        self.deadline.setAlignment(Qt.AlignCenter)
        self.deadline.setTextInteractionFlags(Qt.TextSelectableByKeyboard|Qt.TextSelectableByMouse)
        self.deadline.setObjectName("deadline")
        self.taskWidgetHorizontalLayout.addWidget(self.deadline)

        self.priority = QLabel(self)
        self.priority.setText(priority_)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.priority.sizePolicy().hasHeightForWidth())
        self.priority.setSizePolicy(sizePolicy)
        self.priority.setContextMenuPolicy(Qt.NoContextMenu)
        self.priority.setMinimumSize(QSize(52, 0))
        self.priority.setTextFormat(Qt.PlainText)
        self.priority.setAlignment(Qt.AlignCenter)
        self.priority.setObjectName("priority")
        self.taskWidgetHorizontalLayout.addWidget(self.priority)

        self.nrOfSubtasks = QLabel(self)
        self.nrOfSubtasks.setText("")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nrOfSubtasks.sizePolicy().hasHeightForWidth())
        self.nrOfSubtasks.setSizePolicy(sizePolicy)
        self.nrOfSubtasks.setContextMenuPolicy(Qt.NoContextMenu)
        self.nrOfSubtasks.setMinimumSize(QSize(66, 0))
        self.nrOfSubtasks.setTextFormat(Qt.PlainText)
        self.nrOfSubtasks.setAlignment(Qt.AlignCenter)
        self.nrOfSubtasks.setObjectName("nrOfSubtasks")
        self.taskWidgetHorizontalLayout.addWidget(self.nrOfSubtasks)

        self.enterButton = QToolButton(self)
        self.enterButton.setContextMenuPolicy(Qt.NoContextMenu)
        self.enterButton.setText("...")
        icon = QIcon()
        icon.addPixmap(QPixmap(":/icons/icons/arrow-elbow-down-left.svg"), QIcon.Normal, QIcon.Off)
        self.enterButton.setIcon(icon)
        self.enterButton.setIconSize(QSize(24, 24))
        self.enterButton.setObjectName("enterButton")
        self.taskWidgetHorizontalLayout.addWidget(self.enterButton)

"""
if __name__ == "__main__":

    import sys
    
    app = QApplication(sys.argv)
  
    task = TaskWidget(None, "Objat", "28.11.2021, 16:00")
    task.show()

    sys.exit(app.exec_())
"""