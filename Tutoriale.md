﻿[TOC]

## Wprowadzenie

### Kwestie zasadnicze:

> `Nasz bread & butter.`
> * [Layout Management](https://doc.qt.io/qt-5.15/layout.html)
> * [Qt Designer Manual](https://doc.qt.io/qt-5/qtdesigner-manual.html)

---

### RealPython.

> `To jest ogółem kozak strona do nauki czegokolwiek, co związane z Pythonem. Dużo kolorów i obrazków. Nie trzeba czytać wnikliwie.`
> 1. [Podstawy PyQt](https://realpython.com/python-pyqt-gui-calculator/)
> 1. [Layouty](https://realpython.com/python-pyqt-layout/)
> 1. [Podstawy Qt Designer](https://realpython.com/qt-designer-python/#installing-and-running-qt-designer)

---

<br>

## Nieco głębszy tutorial

> `Chyba najlepsze źródło, jeśli chodzi o proporcję przyswajalności i rzetelności.`
> * [PythonGUIs: Developing PyQt apps with Qt Designer](https://www.pythonguis.com/courses/qt-creator/)

---

<br>

## Dokumentacja

### Qt5


> ### Jedyne potrzebne klasy
>
>> <br>
>>
>> ##### Okno główne i fundamenty
>>  * [QMainWindow](https://doc.qt.io/qt-5.15/qmainwindow.html)
>>
>>
>>  * [QWidget](https://doc.qt.io/qt-5.15/qwidget.html) `<-- Jako obiekt: pierwszy widget (najwyższy w hierarchii) w głównym oknie. Zawiera wszystkie inne elementy GUI. Jako klasa: przodek wszystkiego, co niżej na tej liście. Warto znać jego pola, bo dziedziczy je wszystko i regulują one wiele zachowań, m.in. geometrię.`
>>
>>
>> * [QFrame](https://doc.qt.io/qt-5/qframe.html) `<-- Też "pojemnik" albo placeholder. Jest na nim zbudowane trochę Widgetów, dzięki czemu można łatwo i fajnie ustawić ich wygląd.`
>>
>>
>>  * [Qt](https://doc.qt.io/qt-5.15/qt.html) `<-- "Zbiornik" z jedynymi akceptowanymi wartościami wielu preferencji w wielu klasach. Dla nas najważniejszy jest enum "Alignment", bo pozwala wyrównać zawartość widgeta, np. tekst buttona.`
>>
>> <br>
>>
>> ##### Layouty i scrollowanie
>>  * [QScrollArea](https://doc.qt.io/qt-5.15/qscrollarea.html)
>>
>>
>>  * [QHBoxLayout](https://doc.qt.io/qt-5.15/qhboxlayout.html)
>>
>>
>>  * [QVBoxLayout](https://doc.qt.io/qt-5.15/qvboxlayout.html)
>>
>>
>>  * [QGridLayout](https://doc.qt.io/qt-5.15/qgridlayout.html)
>>
>>
>>  * [QFormLayout](https://doc.qt.io/qt-5.15/qformlayout.html)
>>
>>
>>  * [QSpacerItem](https://doc.qt.io/qt-5.15/qspaceritem.html)
>>
>> <br>
>>
>> ##### Widgety -- kontenery
>>  * [QStackedWidget](https://doc.qt.io/qt-5.15/qstackedwidget.html)
>>
>>
>>  * [QTabWidget](https://doc.qt.io/qt-5.15/qtabwidget.html)
>>
>>
>>  * [QGroupBox](https://doc.qt.io/qt-5.15/qgroupbox.html)
>>
>>
>>  * [QSplitter](https://doc.qt.io/qt-5.15/qsplitter.html)
>>
>>
>>  * [QMenu](https://doc.qt.io/qt-5.15/qmenu.html) `<-- Również kontekstowe.`
>>
>> <br>
>>
>> ##### Widgety -- okna dialogowe
>>  * [QDialog](https://doc.qt.io/qt-5.15/qdialog.html)
>>
>>
>>  * [QMessageBox](https://doc.qt.io/qt-5.15/qmessagebox.html)
>>
>>
>>  * [QInputDialog](https://doc.qt.io/qt-5.15/qinputdialog.html)
>>
>>
>>  * [QFileDialog](https://doc.qt.io/qt-5.15/qfiledialog.html)
>>
>> <br>
>> 
>> ##### Widgety -- zawartość
>>  * [QTableWidget](https://doc.qt.io/qt-5.15/qtablewidget.html)
>>
>>
>>  * [QTreeWidget](https://doc.qt.io/qt-5.15/qtreewidget.html)
>>
>>
>>  * [QCalendarWidget](https://doc.qt.io/qt-5.15/qcalendarwidget.html)
>>
>>
>>  * [QLabel](https://doc.qt.io/qt-5.15/qlabel.html)
>>
>> <br>
>>
>> ##### Widgety -- Funkcje programu i edycja zawartości
>>  * [QAction](https://doc.qt.io/qt-5.15/qaction.html) `<-- Tym się wypełnia menu i toolbary oraz wysyła sygnały.`
>>
>>
>>  * [QActionGroup](https://doc.qt.io/qt-5.15/qactiongroup.html)
>>
>>
>>  * [QWidgetAction](https://doc.qt.io/qt-5.15/qwidgetaction.html)
>>
>>
>>  * [QLineEdit](https://doc.qt.io/qt-5.15/qlineedit.html)
>>
>>
>>  * [QSpinBox](https://doc.qt.io/qt-5.15/qspinbox.html)
>>
>>
>>  * [QComboBox](https://doc.qt.io/qt-5.15/qcombobox.html)
>>
>>
>>  * [QDateTimeEdit](https://doc.qt.io/qt-5.15/qdatetimeedit.html)
>>
>>
>>  * [QCompleter](https://doc.qt.io/qt-5.15/qcompleter.html#details)
>>
>>
>>  * [QPlainTextEdit](https://doc.qt.io/qt-5.15/qplaintextedit.html)
>>
>>
>>  * [QPushButton](https://doc.qt.io/qt-5.15/qpushbutton.html)
>>
>>
>>  * [QCheckBox](https://doc.qt.io/qt-5.15/qcheckbox.html)
>>
>>
>>  * [QRadioButton](https://doc.qt.io/qt-5.15/qradiobutton.html)
>>
>>
>>  * [QButtonGroup](https://doc.qt.io/qt-5.15/qbuttongroup.html)
>>
>> <br>
>>
>> ##### Dodatki do Widgetów
>>  * [QToolTip](https://doc.qt.io/qt-5.15/qtooltip.html)
>
> <br>
>
> ### Kodzenie
> * [Wszystkie rodzaje eventów](https://doc.qt.io/qt-5.15/qevent.html#Type-enum)
>
>
> * [Gotowy mechanizm Undo/Redo](https://doc.qt.io/qt-5.15/qundo.html)

---

<br>

### PyQt5

> `Tylko do podpatrzenia, jaka jest nazwa danej klasy, metody, czy pola, bo nie zawsze są takie same jak w Qt.`
> * [Qt Core](https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtcore/qtcore-module.html)
> * [Qt Widgets](https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtwidgets/qtwidgets-module.html#)
> * [QtGui](https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtgui/qtgui-module.html)

## Przydatne info

<br>

### Ze strony Qt:

* Nie musimy używać `QString`, bo PyQt automatycznie zamieni nasze zmienne typu `str`.
* Sygnały można łączyć z wieloma slotami i wieloma sygnałami (signal chaining).
* Slot może być połączony z wieloma sygnałami.
* W Pythonie `None` jest jak `NULL` w C++
* W PyQt nie można ustawiać właściwości Widgetu setterami i czytać getterami. Można je ustawić przekazując do konstruktora za pomocą zwykłego przypisania `=`, np.:
       act = QAction("&Save",
                      self,
                      shortcut=QKeySequence.Save,
                      statusTip="Save the document to disk",
                      triggered=self.save)
 Można też użyć funkcji `pyqtConfigure()`, jeśli obiekt stworzyliśmy wcześniej. Poniższy kod robi to samo co powyższy.
      act = QAction("&Save", self)
                     
      act.pyqtConfigure(shortcut=QKeySequence.Save,
                        statusTip="Save the document to disk",
                        triggered=self.save)

<br>

### Ze strony PyQt (Riverbank Computing)

<br>

#### `PyQt_PyObject`

"It is possible to enable passing any Python object as a signal argument by specifying `PyQt_PyObject` as the type of argument in its signature. For example:

     finished = pyqtSignal('PyQt_PyObject')

This would normally be used for passing objects where the actual Python type isn’t known."
  


<br>

#### `@pyqtSlot()`

"Although PyQt5 allows any Python method to be used as a slot when connecting signals, it is sometimes necessary to explicitly mark a method as being a Qt slot and to provide a C++ signature for it. PyQt5 provides the `pyqtSlot()` decorator to do this.

    @pyqtSlot(type1, type2..., result)

Parameters:

* `types` – the types that define the C++ signature of the slot. Each type may be a Python type or a string that is the name of a C++ type.

* `result` – the type of the result. May be a Python type or a string that specifies a C++ type. This may only be given as a keyword argument (=...).
  
Examples:
  
> ```python
> from PyQt5.QtCore import QObject, pyqtSlot
>    
>     class Foo(QObject):
>    
>        @pyqtSlot()
>        def foo(self):
>            # C++: void foo()
> 
>        @pyqtSlot(int, str)
>        def foo(self, arg1, arg2):
>            # C++: void foo(int, QString)
> 
>        @pyqtSlot(int, name='bar')
>        def foo(self, arg1):
>            # C++: void bar(int)
> 
>        @pyqtSlot(int, result=int)
>        def foo(self, arg1):
>            # C++: int foo(int)
> 
>        @pyqtSlot(int, QObject)
>        def foo(self, arg1):
>            # C++: int foo(int, QObject *)
> ```