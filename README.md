# MakeMyDay [70% finished]

Language: English.

A prototype of an organizer desktop app, written in Python + PyQt5, as part of the _Team Project_ subject on my CS studies.

## [Video demonstration](https://codeberg.org/VanBruggen/python-MakeMyDay/src/branch/main/demo.mkv)

## Screenshot

![](./makemyday.png)

## Important info:

* In order to run the program, one should run the `mainWindow.py` file.

* Until right before the halt, we weren't tracking our work using Git.
* As for planning and coordination, we were using collaborative Markdown files (`TODO.md`, `Linki.md` and `Tutoriale.md`), which are included in this repo.